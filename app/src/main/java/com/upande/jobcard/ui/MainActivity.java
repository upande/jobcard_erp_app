package com.upande.jobcard.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.upande.jobcard.Model.CommonTask;
import com.upande.jobcard.Model.CommonTaskData;
import com.upande.jobcard.Model.Employee;
import com.upande.jobcard.R;
import com.upande.jobcard.api.RetrofitClientInstance;
import com.upande.jobcard.Model.EmployeeData;
import com.upande.jobcard.services.HttpRequestService;
import com.upande.jobcard.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    Button newJobButton, btnViewJobList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        newJobButton = findViewById(R.id.btnNewJob);
        btnViewJobList = findViewById(R.id.btnViewJobList);
        newJobButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, NewJobActivity.class);
                startActivity(i);
            }
        });
        btnViewJobList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, JobListActivity.class);
                startActivity(i);
            }
        });


        //methods
        getEmployees();
        getCommonTasks();
    }


    //Get a list of all employees
    public void getEmployees() {
        /*Create handle for the RetrofitInstance interface*/
        HttpRequestService service = RetrofitClientInstance.getRetrofitInstance().create(HttpRequestService.class);
        SharedPreferences shared = getSharedPreferences("sid_value", MODE_PRIVATE);
        String sid = shared.getString("sid", "test");
        Call<EmployeeData> call = service.getEmployees(sid, "/", "application/json");
        call.enqueue(new Callback<EmployeeData>() {
            @Override
            public void onResponse(Call<EmployeeData> call, Response<EmployeeData> response) {

                if (response.code() == 200) {
                    Constants.employeeList.clear();
                    List<Employee> employeeData = response.body().getData();
                    for (int i = 0; i < employeeData.size(); i++) {
                        Constants.employeeList.add(employeeData.get(i).getEmployeeName());
                    }
                } else {
                    Log.e("Error", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<EmployeeData> call, Throwable t) {
//                progressDoalog.dismiss();
                Toast.makeText(MainActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }




    //Get a list of all common tasks
    public void getCommonTasks() {
        /*Create handle for the RetrofitInstance interface*/
        HttpRequestService service = RetrofitClientInstance.getRetrofitInstance().create(HttpRequestService.class);
        SharedPreferences shared = getSharedPreferences("sid_value", MODE_PRIVATE);
        String sid = shared.getString("sid", "_");
        Call<CommonTaskData> call = service.getCommonTasks(sid, "/", "application/json");
        call.enqueue(new Callback<CommonTaskData>() {
            @Override
            public void onResponse(Call<CommonTaskData> call, Response<CommonTaskData> response) {

                if (response.code() == 200) {
                    Constants.commonTasksList.clear();
                    List<CommonTask> commonTasksData = response.body().getData();
                    for (int i = 0; i < commonTasksData.size(); i++) {
                        Constants.commonTasksList.add(commonTasksData.get(i).getName());
                    }
                } else {
                    Log.e("Error", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<CommonTaskData> call, Throwable t) {
//                progressDoalog.dismiss();
                Toast.makeText(MainActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
