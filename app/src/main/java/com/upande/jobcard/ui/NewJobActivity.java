package com.upande.jobcard.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.os.Bundle;
import android.view.View;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.upande.jobcard.R;
import com.upande.jobcard.utils.Constants;

import java.util.Calendar;
import java.util.List;

import studio.carbonylgroup.textfieldboxes.ExtendedEditText;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;

public class NewJobActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{
    TextFieldBoxes mAssignedTo, mDueDate, mCategory;
    ExtendedEditText mAssignedToEditText, mDueDateEditText, mCategoryEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_job);

        //Initialization
        mAssignedTo = findViewById(R.id.assignedTo_boxes);
        mAssignedToEditText = findViewById(R.id.assignedTo_et);
        mDueDate = findViewById(R.id.duedate_boxes);
        mDueDateEditText = findViewById(R.id.duedate_et);
        mCategory = findViewById(R.id.category_boxes);
        mCategoryEditText = findViewById(R.id.category_et);


        //Click listeners
        mAssignedTo.setOnClickListener(view -> popupItems(mAssignedTo, mAssignedToEditText, Constants.employeeList));
        mDueDate.setOnClickListener(view -> showTimePickerDialog());
        mCategory.setOnClickListener(view -> popupItems(mCategory, mCategoryEditText, Constants.commonTasksList));

    }


    private void showTimePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                NewJobActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");

    }

    //new onDateSet
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String startDate = year + "-"+ monthOfYear + "-" + dayOfMonth;
        String endDate = yearEnd+ "-"+ monthOfYearEnd + "-" + dayOfMonthEnd;
        mDueDateEditText.setText(endDate);
    }



//    Populate array items to edittext
    private void popupItems(TextFieldBoxes textfield, ExtendedEditText editText, List<String> items) {
        PopupMenu p = new PopupMenu(NewJobActivity.this, textfield);
        for (String s : items) {
            p.getMenu().add(s);
        }
        p.setOnMenuItemClickListener(item -> {
            editText.setText(item.getTitle());
            return true;
        });
        p.show();
    }

}
