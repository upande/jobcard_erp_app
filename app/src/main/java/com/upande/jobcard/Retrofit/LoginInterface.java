package com.upande.jobcard.Retrofit;

import com.upande.jobcard.Model.LoginModel;
import com.upande.jobcard.Model.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginInterface {
    @FormUrlEncoded
   // @Headers("Content-Type:application/json")
    @POST("/api/method/login")
    Call<LoginResponse>  userLogin(
            @Header("Accept") String accept,
            @Field("usr") String email,
            @Field("pwd") String password
    );
}
