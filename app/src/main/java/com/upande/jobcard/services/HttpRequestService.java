package com.upande.jobcard.services;

import com.upande.jobcard.Model.CommonTask;
import com.upande.jobcard.Model.CommonTaskData;
import com.upande.jobcard.Model.EmployeeData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

public interface HttpRequestService {

    @Headers("Accept: application/json")
    @GET("/api/resource/Employee?fields=[\"employee_name\",\"employee\",\"employment_type\",\"gender\"]")
    Call <EmployeeData> getEmployees(@Header("Cookie") String sid, @Header("path") String path, @Header("Content-Type") String content_type);

    @Headers("Accept: application/json")
    @GET("/api/resource/Common%20Tasks?fields=[\"name\",\"active\",\"type_of_project\",%20\"owner\"]")
    Call <CommonTaskData> getCommonTasks(@Header("Cookie") String sid, @Header("path") String path, @Header("Content-Type") String content_type);



}
