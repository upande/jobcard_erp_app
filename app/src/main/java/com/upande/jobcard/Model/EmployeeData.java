package com.upande.jobcard.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmployeeData {

    @SerializedName("data")
    @Expose
    private List<Employee> data = null;

    public EmployeeData(List<Employee> data) {
        this.data = data;
    }

    public List<Employee> getData() {
        return data;
    }

    public void setData(List<Employee> data) {
        this.data = data;
    }
}
