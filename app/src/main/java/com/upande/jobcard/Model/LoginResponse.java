package com.upande.jobcard.Model;

public class LoginResponse {
    private String homepage;
    private String message;
    private String fullname;
    private LoginModel loginModel;



    public LoginResponse(String homepage, String message, String fullname, LoginModel loginModel) {
        this.homepage = homepage;
        this.message = message;
        this.fullname = fullname;
        this.loginModel = loginModel;
    }

    public String getHomepage() {
        return homepage;
    }

    public String getMessage() {
        return message;
    }

    public String getFullname() {
        return fullname;
    }
    public LoginModel getLoginModel() {
        return loginModel;
    }
}
