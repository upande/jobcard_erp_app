package com.upande.jobcard.Model;

import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("usr")
    public String username;
    @SerializedName("pwd")
    public String password;

    public LoginModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
