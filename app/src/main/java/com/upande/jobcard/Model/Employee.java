package com.upande.jobcard.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Employee {

    @SerializedName("employee")
    @Expose
    private String employee;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("employment_type")
    @Expose
    private String employmentType;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
//
//    public Employee(String employee, String gender, String employmentType, String employeeName) {
//        this.employee = employee;
//        this.gender = gender;
//        this.employmentType = employmentType;
//        this.employeeName = employeeName;
//    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

}
