package com.upande.jobcard.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommonTaskData {
    @SerializedName("data")
    @Expose
    private List<CommonTask> data = null;

    public List<CommonTask> getData() {
        return data;
    }

    public void setData(List<CommonTask> data) {
        this.data = data;
    }
}
